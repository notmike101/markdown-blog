# README #

A simple blog system that uses markdown parsed files to render blog posts.

### How do I get set up? ###

* Clone repository
* Make markdown files in the configured directory

### Contribution guidelines ###

* All code must be fully documented to be approved
* Pull requests must describe in detail what they are doing

### Who do I talk to? ###

* @notmike101

### Resources ###
* [ParseDown by Erusev](https://github.com/erusev/parsedown)